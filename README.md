# Trendyol Case2

See progress on: https://hackmd.io/@linuxgemini/Hy7kbZ6bu

Manual configuration for Grafana is required, data source (which is http://prometheus:9090 within the docker-compose network) must be added manually.

Grafana graph JSON is included.

![](https://btw.i-use-ar.ch/i/lgvejlfa.png)

